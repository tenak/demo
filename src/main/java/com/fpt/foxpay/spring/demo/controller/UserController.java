package com.fpt.foxpay.spring.demo.controller;

import com.fpt.foxpay.spring.demo.model.database.User;
import com.fpt.foxpay.spring.demo.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("getAll")
    public ResponseEntity getAllUser() {
        return userService.getAllUser();
    }

    @GetMapping("get/{id}")
    public ResponseEntity getUserById(@PathVariable("id") UUID user_id) {
        return userService.getUserById(user_id);
    }

    @PostMapping("update")
    public ResponseEntity updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity deleteUserById(@PathVariable("id") UUID user_id) {
        return userService.deleteUser(user_id);
    }
}
