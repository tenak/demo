package com.fpt.foxpay.spring.demo.repository;

import com.fpt.foxpay.spring.demo.model.database.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findAllByActiveTrue();

    Optional<User> findByUserIdAndActiveTrue(UUID user_id);
}
