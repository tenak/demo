package com.fpt.foxpay.spring.demo.model;

import lombok.Data;

@Data
public class ErrorDTO {

    private String error;

    private String error_description;


    public static ErrorDTO newError(String error, String desc) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setError(error);
        errorDTO.setError_description(desc);
        return errorDTO;
    }

}
