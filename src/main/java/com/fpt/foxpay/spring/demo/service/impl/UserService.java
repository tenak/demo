package com.fpt.foxpay.spring.demo.service.impl;

import com.fpt.foxpay.spring.demo.model.ErrorDTO;
import com.fpt.foxpay.spring.demo.model.SuccessDTO;
import com.fpt.foxpay.spring.demo.model.database.User;
import com.fpt.foxpay.spring.demo.repository.UserRepository;
import com.fpt.foxpay.spring.demo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public ResponseEntity updateUser(User user) {
        userRepository.save(user);
        return ResponseEntity.ok(SuccessDTO.getDefault());
    }

    @Override
    public ResponseEntity getAllUser() {
        return ResponseEntity.ok(userRepository.findAllByActiveTrue());
    }

    @Override
    public ResponseEntity getUserById(UUID user_id) {
        Optional<User> userOptional = userRepository.findByUserIdAndActiveTrue(user_id);
        if (userOptional.isPresent())
            return ResponseEntity.ok(userOptional.get());
        return ResponseEntity.ok(ErrorDTO.newError("user_not_found", "user_not_found"));
    }

    @Override
    public ResponseEntity deleteUser(UUID user_id) {
        Optional<User> userOptional = userRepository.findById(user_id);
        if (userOptional.isPresent()) {
            userRepository.deleteById(user_id);
            return ResponseEntity.ok(SuccessDTO.getDefault());
        }
        return ResponseEntity.ok(ErrorDTO.newError("user_not_found", "user_not_found"));
    }
}
