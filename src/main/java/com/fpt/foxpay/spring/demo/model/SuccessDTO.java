package com.fpt.foxpay.spring.demo.model;

import lombok.Data;
import org.aspectj.lang.annotation.DeclareAnnotation;

@Data
public class SuccessDTO {

    private boolean success;

    public static SuccessDTO getDefault() {
        SuccessDTO successDTO = new SuccessDTO();
        successDTO.setSuccess(true);

        return successDTO;
    }

}