package com.fpt.foxpay.spring.demo.service;

import com.fpt.foxpay.spring.demo.model.database.User;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface IUserService {

    ResponseEntity updateUser(User user);

    ResponseEntity getAllUser();

    ResponseEntity getUserById(UUID user_id);

    ResponseEntity deleteUser(UUID user_id);
}
